# Read Me:#
engOrg is team for the class CS461/CS462 which is a senior project for 2016/2017 at Western Oregon University. 

## What is  myWall?
An application used to collaborate with your team, your co-worker, your friends, and other people that you share the same interests with. In myWall app, you can have a learning experience with classmates, friends and teachers. This app provides several tools that are easy to use and useful for your needs such as drawing diagrams, making lists, posting documents, ...etc. The focus is to have an amazing place to share your ideas, comments, knowledge, and other things with the right people at the right time.

## Envision Statement ##
myWall is a collaboration tool for working with collogues locally or remotely where users can draw out diagrams, mind maps, brainstorming lists, flow charts, etc. and save them for later review. myWall is a web application that allows users to connect with other users whether it is school, business, art, software engineering projects, the individuals using will be able to display their creativity with their very own virtual whiteboard. The system will show real-time updates as others draw, either using digital whiteboards or tablets, and areas of the white board can be protected with the option of turning on or off overlays of others drawings or notes. It is intended to be for both individual users and for teams who need a way to work collaboratively and have an easy way to keep track of their progress. 
Unlike A Web Whiteboard (awwapp.com), www.webwhiteboard.com, or Realtime Board (realtimeboard.com). Our app will be different as there will be a section where items can be posted to the community at large, like a Stack Exchange site.




## Team Members:##

* Ahmed Almutairi
* Whitney Meulink
* Robert Balsley

## Tools that are used in developing the project  ##

* Visual Studio MVC5 v2015
* SQL Server 2015
* Linq pad 5
* [Visual Studio Team Service](https://engorg.visualstudio.com/theWall/_backlogs?level=Epics&_a=backlog)
* [Bitbucket](https://bitbucket.org/CS460Almutairi/engorg)
* [Azure Deployed site](http://neonews.azurewebsites.net/)

##Needs to be installed from Nutget:##
* Ninject V3.2.2
* Ninject.MVC5 V3.2.1
* Ninject.Web.Common V3.2.3
* Moq V4.5.30